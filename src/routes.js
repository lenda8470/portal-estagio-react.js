import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/login';
import Cad from './pages/cadaluno';
import Acom from './pages/acomtarefas';
import Error404 from './templates/Error404';
import Email from './pages/telaEditEmail';
import Dados from './pages/telaEditDados';

export default function Routes() {
   return (
      <BrowserRouter>
         <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/projetos" component={Acom} />
            <Route path="/cad-aluno" component={Cad} />
            <Route path="/edit-email" component={Email} />
            <Route path="/edit-dados" component={Dados} />
            <Route path="*" component={Error404} />            
         </Switch>
      </BrowserRouter>
      
   )
}