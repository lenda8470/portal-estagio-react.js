import React from 'react'
import './styles.css'
export default function cadastro() {
   return (
      <>
         <div className="cadastro">
            <h1>Cadastrar Aluno</h1>

            <div className="area_inputs">
               <div className="line_inputs">
                  <div className="input_group">
                     
                     <label>Imagem para perfil:</label>
                     <input type="file" placeholder="nenhum arquivo selecionado" />

                  </div>

                  <div className="input_group">
                     <label>Nome completo:</label>
                     <input type="email" placeholder="Digite seu nome completo" />
                  </div>
               </div>

               <div className="line_inputs">
                  <div className="input_group">
                     <label>Instituição</label>
                     <select id="eeeps" name="op">
                        <option value="EEEP MONSENHOR JOSE ALOYSIO PINTO"> E.E.E.P MONSENHOR JOSE ALOYSIO PINTO</option>
                        <option value="UNINTA">UNINTA  </option>
                        <option value="EEEP DOM WALFRIDO TEIXEIRA VIEIRA">E.E.E.P DOM WALFRIDO TEIXEIRA VIEIRA</option>
                        <option value="EEEP DOM WALFRIDO TEIXEIRA VIEIRA"> FIED </option>
                        <option value="EEEP DOM WALFRIDO TEIXEIRA VIEIRA"> E.E.E.P PROFESSORA LISYA PIMENTEL GOMES SALES </option>
                     </select>
                  </div>

                  <div className="input_group">
                     <label>Cursos:</label>
                     <select id="eeeps" name="op">
                        <option value="EEEP MONSENHOR JOSE ALOYSIO PINTO"> Administração </option>
                        <option value="UNINTA"> Jornalismo </option>
                        <option value="EEEP DOM WALFRIDO TEIXEIRA VIEIRA"> Sistema de Informação </option>
                        <option value="EEEP DOM WALFRIDO TEIXEIRA VIEIRA"> Tecnico em informática </option>
                     </select>
                  </div>
               </div>
               <div className="line_inputs">
                  <div className="input_group">
                     <label>Nº Registro acadêmico:</label>
                     <input type="password" placeholder="Digite seu registro" />
                  </div>

                  <div className="input_group">
                     <label>Celular:</label>
                     <input type="text" placeholder="Digite seu número de celular" />
                  </div>
               </div>
               <div className="line_inputs">
                  <div className="input_group">
                     <label>Email:</label>
                     <input type="text" placeholder="Digite seu E-mail" />
                  </div>

                  <div className="input_group">
                     <label>Senha:</label>
                     <input type="password" placeholder="Digite sua senha" />
                  </div>
               </div>

            </div>


            <div className="buttons_actions">
               <center>
               <button type="button" className="success" >Cadastrar</button></center>
               <center>
               <button type="button" className="info" >Entrar na plataforma!</button></center>
               
            </div>
         </div>

      </>
   )
}
