import React from 'react'
import { FaEye } from "react-icons/fa";

import './styles.css';

export default function projetos() {
   return (
      <>
         <div className="projetos">
            <h1>Acompanhamento de estágios</h1>
          
            <table style={{ width: '90%' }}>
               <thead style={{ background: '#bb0f20', color: '#fff' }}>
                  <tr>
                     <th>Projeto</th>
                     <th>Supervisor</th>
                     <th className="line_action">Ações</th>
                  </tr>
               </thead>

               <tbody>
                  <tr>
                     <td>
                        Estágio Desenvolvimento FRONT-END 2021.1 - DIRIN
                     </td>
                     <td>
                        Maycon Douglas
                     </td>
                     <td>
                        <FaEye className="olho" style={{ color: '#bb0f20' }}/>
                     </td>
                  </tr>
               </tbody>
          </table>
         </div>
      </>
   )
}
