import React from 'react'
import { useHistory } from 'react-router-dom';
import { MdEmail, MdLock } from "react-icons/md";
import './login.css'
export default function Login() {
   const history = useHistory();

   const logar = () => {
      sessionStorage.setItem('credenciais', JSON.stringify({ access: 1 }));

      history.push('/projetos');
   }

   return (
      <div className="login">
         <div className="login-right">
            <h1>Gestão de Estágio </h1>
            <p>
               <b> Faça Login para continuar    </b>
            </p>
            <div>
               <div className="login-loginInputEmail">
                  <MdEmail/>
                  <input type="email"
                     placeholder="Digite seu email"
                  />
               </div>
               <div className="login-loginInputPassword">
                  <MdLock/>
                  <input type="password"
                     placeholder="Digite sua senha"
                  />
               </div>
              
            </div>
            <button type="button" onClick={() => {logar()}}>
               ENTRAR
            </button>
         </div>

      </div>
   )
}

