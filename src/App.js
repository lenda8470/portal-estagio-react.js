import React,{Fragment} from 'react'
import Router from './routes'
import Header from './components/Header'
import Footer from './components/Footer'


function App() {
  return (
    <Fragment style={{position:'relative'}}>
      <Header />
      <Router />
      <Footer/>
    </Fragment>
  );
}
export default App;
