import React, { useState } from 'react';
import { BsHouseDoor } from "react-icons/bs";
import { FaUserCircle } from "react-icons/fa";
import './styles.css';

export default function Header() {
 
   const [modal, setModal] = useState(false);

   const logoou = JSON.parse(sessionStorage.getItem('credenciais'));

   return (
      <>
         <div className='header_page_login'>
            <img className="logo_img" src="/aiamis.png" alt="" />
            {
               logoou?.access === 1 ? (
                  <ul>
                     <li className="ini" style={{ color: 'white' }}>início</li>
                     <BsHouseDoor className="icon" style={{ color: 'white' }} />
                  </ul>
               ) : null
            }
            <button onClick={() => { setModal(true); }} className="btn">
               🠸VITOR DERIER DA SILVA LOIOLA <FaUserCircle className="icon_user" />
            </button>
         </div>
            
         <div className={modal ? "modal" : "modal_close"}>
            <div className="body_modal">
               <button onClick={() => { setModal(false) }} className="btn_fechar">
                  X
               </button>

               <h1 className="text_modal" style={{ color: 'white' }}></h1>

               <button className="email_senha">
                  Alterar E-mail e Senha
               </button>
               <button className="alter_dados">
                  Alterar dados da conta
               </button>
               <hr className="linha"></hr>
               <form action="/" method="GET"> 
               <button className="sair_plat" >
               Sair da plataforma 
               </button>
               </form>
            </div>
         </div>
      </>
   )
}