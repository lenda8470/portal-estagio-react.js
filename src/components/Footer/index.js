import React from 'react';
import './styles.css';
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css" integrity="sha384-wESLQ85D6gbsF459vf1CiZ2+rr+CsxRY0RpiF1tLlQpDnAgg6rwdsUF1+Ics2bni" crossorigin="anonymous"></link>

export default function Footer() {
   return (
      <div className='footer_page'>
         <div className="row_footer">
            <div className="block">
               <img className="logo_immg"  src="/aiamis.png" alt="" />

               <span>
                  <p className="title_project" style={{ fontSize: '1.6rem' }}>
                     - Portal Estágio -
                  </p>
                  <p className="title_project">
                     Acompanhamento de atividades práticas
                     <img className="logo_immg"  src="/" alt="" />
                  </p>
               </span>
            </div>

            <div className="block">
           
               <h2>AIAMIS</h2>

               <ul className="list_footer">
                  <a href="https://uninta.edu.br/" class="text-warning">Centro Univeristário INTA - UNINTA</a>
                  <li>
                     <a href="http://faculdadealencarina.com.br/site/" class="text-warning">Faculdade Alencarina de Sobral</a>
                  </li>
                  <li>
                     <a href="http://fnt.edu.br/itapipoca/" class="text-warning">Faculdade Novo Tempo</a>
                  </li>
               </ul>
            </div>

            <div className="block">
               <h2 className="ctts">Entre em Contato</h2>

               <ul className="list_footer">
                  <li style={{ textAlign: 'left' }}>
                     (88) 3112 3500
                  </li>
                  <li style={{ textAlign: 'left' }}>
                     (88) 9 9795 2882
                  </li>
                  <li style={{ textAlign: 'left' }}>
                     ouvidoria@uninta.edu.br
                  </li>
                  <li style={{ textAlign: 'left' }}>
                     <a href="http://fnt.edu.br/itapipoca/" class="text-warning">Torne-se um polo uninta</a>
                  </li>
               </ul>
            </div>

            <div className="block">
               <h2 className="nome_canais">Canais de comunicação</h2>
               <p>Siga nossos canais de comunicação</p>

            </div>
         </div>

         <div className="copyright" align="center">
            <p>© Diretoria de Inovação Educacional AIAMIS | Todos os direitos reservados. - Privacidade e condições de usos. 2020.</p>
         </div>
      </div>

   )
}